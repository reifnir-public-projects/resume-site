variable "azure_client_id" {
  description = "ACME challenge Azure Client ID (think username)"
  type        = string
}

variable "azure_client_secret" {
  description = "ACME challenge Azure Client Secret (think password)"
  sensitive   = true
  type        = string
}

variable "azure_subscription_id" {
  description = "Azure Subscription to create resources in."
  type        = string
}

# We're hosting DNS with an Azure DNS zone
variable "azure_dns_zone_id" {
  description = "The full Azure resource ID for the DNS zone we're using."
  type        = string
}

variable "old_azure_dns_zone_id" {
  description = "We're going to be making some redirects from here to the `azure_dns_zone_id` root"
  type        = string
}

variable "lets_encrypt_contact_email" {
  description = "Who should be emailed when the cert is close to expiring?"
  type        = string
}

variable "gitlab_token" {
  description = "Token with permissions to make changes in Gitlab."
  type        = string
}

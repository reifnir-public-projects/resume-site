locals {
  tags = {
    "Application" = "Hopefully, the cheapest, easiest hosting I've done so far"
    "ManagedBy"   = "Terraform"
  }
}

# Magic up the static site...
module "resume_andreasen_dev" {
  # source                   = "../../terraform-azurerm-static-site/"
  source                   = "reifnir/static-site/azurerm"
  name                     = "andreasen-dev-resume"
  static_content_directory = "${path.root}/../content"

  custom_dns = {
    # @ is interpreted as a naked domain. Ex: example.com
    hostnames                  = ["@", "www", "resume"]
    dns_provider               = "azure"
    dns_zone_id                = var.azure_dns_zone_id
    lets_encrypt_contact_email = var.lets_encrypt_contact_email
    azure_client_id            = var.azure_client_id
    azure_client_secret        = var.azure_client_secret
  }

  tags = local.tags
}

module "resume_reifnir_com" {
  # source                   = "../../terraform-azurerm-static-site/"
  source                   = "reifnir/static-site/azurerm"
  name                     = "reifnir-com-resume"
  static_content_directory = "${path.root}/../content"

  custom_dns = {
    # @ is interpreted as a naked domain. Ex: example.com
    hostnames                  = ["@", "www", "resume"]
    dns_provider               = "azure"
    dns_zone_id                = var.old_azure_dns_zone_id
    lets_encrypt_contact_email = var.lets_encrypt_contact_email
    azure_client_id            = var.azure_client_id
    azure_client_secret        = var.azure_client_secret
  }

  tags = local.tags
}

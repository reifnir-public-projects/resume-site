# If it doesn't vary, don't make it a variable
data "gitlab_project" "resume_site" {
  path_with_namespace = "reifnir-public-projects/resume-site"
}

# This won't renew every pipeline, it only does the change when the certs within 30 days of expiring
resource "gitlab_pipeline_schedule" "renew" {
  project     = data.gitlab_project.resume_site.id
  description = "Automatically renew Let's Encrypt certs"
  ref         = "main"
  cron        = "0 16 1 * *" # first of each month at 4pm
}

terraform {
  backend "azurerm" {
    # Relies on environment variables for configuration
    resource_group_name  = "rg-common-storage"
    storage_account_name = "sareifnircommonstorage"
    container_name       = "terraform-state"
    key                  = "resume-site.tfstate"
  }
  required_version = ">= 1.2"
  required_providers {
    acme = {
      source = "vancluever/acme"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

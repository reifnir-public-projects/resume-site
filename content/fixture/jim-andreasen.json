{
    "ContactInfo": {
        "FirstName": "Jim",
        "LastName": "Andreasen",
        "Email": "jim@andreasen.dev",
        "Web": "https://andreasen.dev"
    },
    "Skills": {
        "Technologies": [
            {
                "Name": "Cloud",
                "Children": [
                    {
                        "Name": "Amazon Web Services",
                        "Url": "https://aws.amazon.com/"
                    },
                    {
                        "Name": "Microsoft Azure",
                        "Url": "https://azure.microsoft.com/en-us/"
                    }
                ]
            },
            {
                "Name": "Infrastructure as Code (IaC)",
                "Children": [
                    {
                        "Name": "Terraform",
                        "Url": "https://www.terraform.io/"
                    },
                    {
                        "Name": "CloudFormation",
                        "Url": "https://aws.amazon.com/cloudformation/"
                    },
                    {
                        "Name": "Puppet",
                        "Url": "https://puppet.com/"
                    }
                ]
            },
            {
                "Name": "Distributed Computing",
                "Children": [
                    {
                        "Name": "Kubernetes (EKS, AKS, vanilla)",
                        "Url": "https://kubernetes.io"
                    },
                    {
                        "Name": "Service Fabric",
                        "Url": "https://azure.microsoft.com/en-us/services/service-fabric/"
                    },
                    {
                        "Name": "Apprenda",
                        "Url": "https://apprenda.com/"
                    },
                    {
                        "Name": "NServiceBus 4-5",
                        "Url": "http://particular.net/nservicebus"
                    }
                ]
            },
            {
                "Name": "Languages",
                "Children": [
                    {
                        "Name": "C# 1.1 - 9.0",
                        "Url": "http://msdn.microsoft.com/en-us/library/ms731082.aspx"
                    },
                    {
                        "Name": "Bash"
                    },
                    {
                        "Name": "JavaScript"
                    },
                    {
                        "Name": "PowerShell"
                    },
                    {
                        "Name": "Python"
                    },
                    {
                        "Name": "T-SQL"
                    }
                ]
            },
            {
                "Name": "Misc",
                "Children": [
                    {
                        "Name": "Linux (Ubuntu, Debian, CentOS, Amazon Linux, Kali)"
                    },
                    {
                        "Name": "Gitlab CI/CD",
                        "Url": "https://docs.gitlab.com/ee/ci/"
                    },
                    {
                        "Name": "Docker Swarm",
                        "Url": "https://docs.docker.com/engine/swarm/"
                    },
                    {
                        "Name": "Azure Functions",
                        "Url": "https://azure.microsoft.com/en-us/services/functions"
                    },
                    {
                        "Name": "AWS Lambda",
                        "Url": "https://aws.amazon.com/lambda/"
                    },
                    {
                        "Name": "GitHub Actions",
                        "Url": "https://github.com/features/actions"
                    },
                    {
                        "Name": "WCF",
                        "Url": "http://msdn.microsoft.com/en-us/library/ms731082.aspx"
                    }
                ]
            }
        ],
        "Methods": [
            {
                "Name": "Software Architecture Design",
                "Children": [
                    {
                        "Name": "Volatility-based decomposition",
                        "Url": "https://www.youtube.com/watch?v=VIC7QW62-Tw"
                    },
                    {
                        "Name": "SOLID design principles",
                        "Url": "https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)"
                    },
                    {
                        "Name": "Project design",
                        "Url": "http://www.idesign.net/Training/Project-Design-Master-Class"
                    },
                    {
                        "Name": "Distributed system architecture"
                    },
                    {
                        "Name": "Cloud, on-prem and hybrid hosting models"
                    },
                    {
                        "Name": "Technical and business analysis"
                    },
                    {
                        "Name": "Focus on total cost of ownership"
                    },
                    {
                        "Name": "Mentorship"
                    }
                ]
            },
            {
                "Name": "Training",
                "Children": [
                    {
                        "Name": "IDesign Architect's Master Class (Juval Lowy)",
                        "Url": "http://www.idesign.net/Training/Architect-Master-Class"
                    },
                    {
                        "Name": "IDesign Project Design Master Class (Juval Lowy)",
                        "Url": "http://www.idesign.net/Training/Project-Design-Master-Class"
                    },
                    {
                        "Name": "IDesign Architecture Clinic (Michael 'Monty' Montgomery)",
                        "Url": "http://www.idesign.net/Clinics/Architecture-Clinic"
                    },
                    {
                        "Name": "IDesign Detailed Design Clinic (Michael 'Monty' Montgomery) March 2017",
                        "Url": "http://www.idesign.net/Clinics/Detailed-Design-Clinic"
                    },
                    {
                        "Name": "IDesign Service Fabric Master Class (Michael 'Monty' Montgomery) May 2019",
                        "Url": "http://idesign.net/Training/Service-Fabric-Master-Class"
                    },
                    {
                        "Name": "Agile project management (Rally)",
                        "Url": "http://www.rallydev.com/services/ready-set-sprint"
                    }
                ]
            }
        ]
    },
    "WorkExperience": [
        {
            "CompanyName": "Kroll Bond Rating Agency",
            "StartDate": "2019-11-06T00:00:00",
            "Title": "Sr. Staff Software Engineer",
            "Address": "Fully Remote",
            "Description": [
                {
                    "Text": "Responsible for offering guidance as well as solutions to cross-cutting concerns to the many engineering teams we supported."
                },
                {
                    "Text": "That guidance came in many forms. All the way from helping teams troubleshoot \"why does my script have a failing exit code when nothing seems wrong?\" to helping them get their minds around the main concepts and getting started working with Gitlab automation, Terraform, Docker, etc. Several times, that guidance was an intense multi-week engagement to understand their problems, what changes over time and together, help them come up with a viable software architecture."
                },
                {
                    "Text": "Inherited several Docker Swarms that were manually created. Stabilized clusters, switched-over to IaC and automated cycling nodes."
                },
                {
                    "Text": "Implemented and automated dozens of other Terraform projects, including the following: managing AWS account creation and governance of over 50 accounts (automated with the exception of 1 well-documented UI interaction), managing access to AWS accounts using SSO through Azure AD, creation of VPCs for each AWS Account and attaching them to Transit Gateways for cross-account communication, creation of both Azure AKS and AWS EKS Kubernetes clusters with managed ingress, provisioning of Fargate profiles and namespaces in EKS, managing RBAC access to both AWS and Azure, provisioning AWS Cloud9 environments and access on demand for interviews, shipping of Azure App Gateway logs to Datadog, managing Tagging policies in Azure, custom Datadog metrics and monitors that triggered Opsgenie alerts, many simple example repos demonstrating solutions for one technique or problem, managed reusable Gitlab templates that work similarly to GitHub Actions for Terraform security scanning, etc."
                },
                {
                    "Text": "We were also responsible for a fleet of Ubuntu and Centos Linux pet servers that we inherited and over time, eliminated, consolidated, or improved IaC and CaC on what remained."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "HCL",
                    "Weight": 0.5
                },
                {
                    "Name": "Bash",
                    "Weight": 0.2
                },
                {
                    "Name": "C#",
                    "Weight": 0.1
                },
                {
                    "Name": "JavaScript",
                    "Weight": 0.1
                },
                {
                    "Name": "Python",
                    "Weight": 0.05
                },
                {
                    "Name": "PowerShell",
                    "Weight": 0.05
                }
            ]
        },
        {
            "CompanyName": "Change Healthcare",
            "StartDate": "2016-06-13T00:00:00",
            "EndDate": "2019-10-18T00:00:00",
            "Title": "Architect",
            "Address": "King of Prussia, PA",
            "Description": [
                {
                    "Text": "Continuation of work started in the course of duties as an Apprenda consultant. Primary responsibility was as internal expert consultant for cloud-enabling existing enterprise products. Delivering DevOps tooling and a provider-based programming model to abstract the platform (essential to supporting existing on-prem customers). Provide individual product teams architectural and technical guidance for PaaS and SaaS-enablment. Additional work: Apigee proxy development, Docker on Apprenda, Kubernetes-related technologies."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "C#",
                    "Weight": 0.3
                },
                {
                    "Name": "PowerShell",
                    "Weight": 0.2
                },
                {
                    "Name": "T-SQL",
                    "Weight": 0.2
                },
                {
                    "Name": "Bash",
                    "Weight": 0.1
                },
                {
                    "Name": "JavaScript",
                    "Weight": 0.1
                },
                {
                    "Name": "Python",
                    "Weight": 0.05
                },
                {
                    "Name": "Java",
                    "Weight": 0.05
                }
            ]
        },
        {
            "CompanyName": "Almac Clinical Technologies",
            "StartDate": "2015-08-01T00:00:00",
            "EndDate": "2016-06-10T00:00:00",
            "Title": "Solutions Architect",
            "Address": "Souderton, PA",
            "Description": [
                {
                    "Text": "Primarily responsible for overseeing multiple development teams, delivering both designs and hands-on solutions for cross-cutting concerns, reducing the cost and time required to prepare and maintain studies for the company's flagship clinical trial management system. Partner with product owners by analyzing business problems, providing viable options and estimations, and facilitate aligning them with the study-level business unit. Collaborate with a small team of fellow architects to ensure consistent guidance and a clear vision for technical solutions. Provide technical leadership and mentoring to development team leads and their subordinates."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "NServiceBus",
                    "Weight": 0.5
                },
                {
                    "Name": "C#",
                    "Weight": 0.4
                },
                {
                    "Name": "T-SQL",
                    "Weight": 0.05
                },
                {
                    "Name": "PowerShell",
                    "Weight": 0.05
                }
            ]
        },
        {
            "CompanyName": "Apprenda, Inc.",
            "StartDate": "2013-11-01T00:00:00",
            "EndDate": "2015-08-01T00:00:00",
            "Title": "Solutions Architect, Client Services",
            "Address": "Troy, NY and King of Prussia, PA",
            "Description": [
                {
                    "Text": "Recommend and design enterprise client solutions for cloud-suitability with consideration for a customer's unique policy, process and technical requirements. Diagnose, troubleshoot and resolve customer issues relating to developer operations, cloud infrastructure and hosted applications. Factor existing applications for SaaS-enablement and distributed hosting on the Apprenda PaaS platform. Develop POCs, lead group training for diverse audiences, and extensively develop and create custom Apprenda platform extensions for cross-app communication and automation."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "WCF",
                    "Weight": 0.50
                },
                {
                    "Name": "C#",
                    "Weight": 0.25
                },
                {
                    "Name": "T-SQL",
                    "Weight": 0.1
                },
                {
                    "Name": "PowerShell",
                    "Weight": 0.1
                },
                {
                    "Name": "PL-SQL",
                    "Weight": 0.05
                }
            ]
        },
        {
            "CompanyName": "MedRisk, Inc.",
            "StartDate": "2013-07-01T00:00:00",
            "EndDate": "2013-11-01T00:00:00",
            "Title": "Sr. Software Engineer",
            "Address": "King of Prussia, PA",
            "Description": [
                {
                    "Text": "Short-term contract to lead migration effort of legacy Access implementation to .NET. Established new platform and standards with architect. Analysis of business needs and current implementation."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "C#",
                    "Weight": 0.65
                },
                {
                    "Name": "SQL",
                    "Weight": 0.35
                }
            ]
        },
        {
            "CompanyName": "Siemens",
            "StartDate": "2013-04-01T00:00:00",
            "EndDate": "2013-07-01T00:00:00",
            "Title": "Lead Software Developer",
            "Address": "Malvern, PA",
            "Description": [
                {
                    "Text": "Short-term contract to deliver work orders for federal regulation compliance. In addition, responsible for training and providing examples to development team with modern principles and techniques of software development."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "C#",
                    "Weight": 0.85
                },
                {
                    "Name": "JavaScript",
                    "Weight": 0.1
                },
                {
                    "Name": "SQL",
                    "Weight": 0.05
                }
            ]
        },
        {
            "CompanyName": "PointRoll, Inc.",
            "StartDate": "2010-07-01T00:00:00",
            "EndDate": "2013-03-01T00:00:00",
            "Title": "Architect, Sr. Software Engineer",
            "Address": "King of Prussia, PA",
            "Description": [
                {
                    "Text": "Rearchitected core application leveraging IoC to break apart a knot of circular dependencies. Lead shaping and implementation of backend design to the core application's more-UX focused reboot as a RESTful, testable API. Acted as a leading contributor to a rule-based dynamic ad content service. Received the peer nominated, management approved Roller (employee) of the year award in 2011."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "C#",
                    "Weight": 0.7
                },
                {
                    "Name": "SQL",
                    "Weight": 0.25
                },
                {
                    "Name": "JavaScript",
                    "Weight": 0.05
                }
            ]
        },
        {
            "CompanyName": "Allied Barton Security Services",
            "StartDate": "2008-03-01T00:00:00",
            "EndDate": "2010-06-01T00:00:00",
            "Title": "Sr. Developer",
            "Address": "Conshohocken, PA",
            "Description": [
                {
                    "Text": "Responsible for designing and replacing legacy vacation/sick accrual and tracking system. The majority of development was done in C# leveraging LINQ, ASP.NET and jQuery driven front end and T-SQL. XSLT, SSIS, SSRS, VB6, and VB.NET 1.1, and LDAP were also used to a lesser extent."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "SQL",
                    "Weight": 0.65
                },
                {
                    "Name": "C#",
                    "Weight": 0.3
                },
                {
                    "Name": "VB6",
                    "Weight": 0.03
                },
                {
                    "Name": "VB.NET",
                    "Weight": 0.02
                }
            ],
            "IsArchived": true
        },
        {
            "CompanyName": "MedRisk, Inc.",
            "StartDate": "2006-10-01T00:00:00",
            "EndDate": "2008-03-01T00:00:00",
            "Title": "Sr. Developer",
            "Address": "King of Prussia, PA",
            "Description": [
                {
                    "Text": "Programming focus was OO and DB design and development. Programming predominantly done in C#, T-SQL, VB6 and Access VBA. Projects include creation of COM Objects/DLLs, web services, automation, stored procedures, query optimization, automated email and attachment capture, and creation of rules based routing engines. In addition to programming: project design, mentoring, requirement gathering from all levels within the organization and technical writing."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "C#",
                    "Weight": 0.60
                },
                {
                    "Name": "SQL",
                    "Weight": 0.30
                },
                {
                    "Name": "VBA",
                    "Weight": 0.07
                },
                {
                    "Name": "VB6",
                    "Weight": 0.03
                }
            ],
            "IsArchived": true
        },
        {
            "CompanyName": "Automated Financial Solutions",
            "StartDate": "2006-01-01T00:00:00",
            "EndDate": "2006-10-01T00:00:00",
            "Title": "Programmer Analyst",
            "Address": "Exton, PA",
            "Description": [
                {
                    "Text": "Programming focus was on VB6, DCOM, T-SQL, and hand-rolled AJAX for loan origination front end that interfaced with mainframes. Projects included: creation and maintenance of COM Objects/DLLs, VBA scripting engine, designed database archival process with on-demand retrieval."
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "VB6",
                    "Weight": 0.8
                },
                {
                    "Name": "SQL",
                    "Weight": 0.1
                },
                {
                    "Name": "XML",
                    "Weight": 0.05
                },
                {
                    "Name": "VBScript",
                    "Weight": 0.05
                }
            ],
            "IsArchived": true
        },
        {
            "CompanyName": "MedRisk, Inc.",
            "StartDate": "2001-06-01T00:00:00",
            "EndDate": "2005-12-01T00:00:00",
            "Title": "Sr. Developer",
            "Address": "King of Prussia, PA",
            "Description": [
                {
                    "Text": "[See description above]"
                }
            ],
            "LanguageConcentration": [
                {
                    "Name": "VBA",
                    "Weight": 0.5
                },
                {
                    "Name": "SQL",
                    "Weight": 0.45
                },
                {
                    "Name": "VB6",
                    "Weight": 0.05
                }
            ],
            "IsArchived": true
        },
        {
            "CompanyName": "Elecor Communications",
            "StartDate": "2000-04-01T00:00:00",
            "EndDate": "2001-02-01T00:00:00",
            "Title": "Network Administrator",
            "Address": "Butler, NJ",
            "Description": [
                {
                    "Text": "As sole member of the technical staff, developed and maintained Classic ASP web site for a digital dictation and document transfer service. Experience in ASP, VBScript, and ActiveX components."
                }
            ],
            "IsArchived": true
        },
        {
            "CompanyName": "Lion Technology",
            "StartDate": "1999-10-01T00:00:00",
            "EndDate": "2000-04-01T00:00:00",
            "Title": "Computer Service Technician",
            "Address": "Lafayette, NJ",
            "Description": [
                {
                    "Text": "Programming experience in Perl, Visual Basic, and HTML."
                }
            ],
            "IsArchived": true
        }
    ],
    "UpdateDate": "2015-03-23T23:08:40",
    "Id": "jim-andreasen"
}
